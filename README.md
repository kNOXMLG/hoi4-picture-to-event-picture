# imgtoeventpic
 Takes a PNG of any size, and turns it into a dds file accessable in HOI4.

 ![test](https://i.ibb.co/PzTtPss/hosp-psiq-14.png) into ![test2](https://i.ibb.co/WWXnQQS/report-event-MEX-91.png)

# How to use
 ### download the models [here](https://drive.google.com/drive/folders/1CCOBcfh8oiQt9DM152bWVfwi9vNjzkrp?usp=sharing) and place them in /model_data

 place all your images into /imagestoprocess

 ### this is a python project, I higly recommend creating a  virtual environment. you might have to change the paths that files load in and save if you dont.
 ## to install required modules ( make sure you cd to the project folder)
 ```powershell
 python -m pip install -r requirements.txt
 ```
 Open imgtocroppedfaces.py and run. Your images will show up in finalimages as dds's.

 ###### You can alternatively set a folder location in overlayimgoncard.py and call imagesbyfolder(). If you do this, make sure your images are already pre cut to widths/heights <150px


